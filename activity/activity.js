/* ACTIVITY S30 */

/* No.2 */
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}
]);


/* No.3 */
db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$count: "enoughStock"}
]);

/* No.4 */
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
	{$sort: {avg_price: -1}}
]);

/* No. 5 */
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
	{$sort: {avg_price: -1}}
]);

/* No. 6 */
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
	{$sort: {avg_price: -1}}
]);

/* End of Activity S30*/